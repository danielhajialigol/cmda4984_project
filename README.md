# CMDA 4984 Final Project

## Usage
This project includes experiments that capture the run times of the forward() method of a convolutional layer in base C++ and in CUDA. To run the experiment, execute

```bash
make cppConv
```

followed by

```bash
make cudaConv
```




