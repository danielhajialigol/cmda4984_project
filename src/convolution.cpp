#include <cstdio>
#include <stdlib.h>
#include <time.h>

#define KERNEL_SIZE 16
#define INPUT_WIDTH 1024
#define INPUT_HEIGHT 768

class ConvLayer{
  public:
  int n_kernels;
  int in_width;
  int in_height;
  int in_channels;
  int kernel_size;
  int filter_size;
  int bias_size;
  int out_width;
  int out_height;
  int out_channels;
  double *K;
  double *B;

public:
  ConvLayer(int num_kernels, int in_rows, int in_cols, int in_depth, int k_size){
    // number of kernels = output depth
    this -> n_kernels = num_kernels;
    
    // dimensions of input
    this -> in_width = in_rows;
    this -> in_height = in_cols;
    this -> in_channels = in_depth;

    // height and width of kernel (assumes square kernel)
    this -> kernel_size = k_size;

    // dimensions of output
    this -> out_width = in_width - kernel_size + 1;
    this -> out_height = in_height - kernel_size + 1;
    this -> out_channels = num_kernels;

    // find size of filter (viewed as one dimensions as opposed to 4)
    this -> filter_size = n_kernels * in_channels * kernel_size * kernel_size;
    this -> bias_size = out_channels * out_height * out_width;
    
    // initialize kernel and bias
    initialize_kernel();
    initialize_bias();
  }

  // method to initialize kernel
  void initialize_kernel(){

    // initialize kernel
    K = (double *) calloc(filter_size, sizeof(double));

    // iterate over the kernel size
    for (int i = 0; i < filter_size; i++){
      
      // assign random values to each element in the kernel
      K[i] = i;//drand48();
    }
  }


  // method to initialize kernel
  void initialize_bias(){

    // initialize kernel
    B = (double *) calloc(bias_size, sizeof(double));

    // iterate over the kernel size
    for (int i = 0; i < bias_size; i++){
      
      // assign random values to each element in the kernel
      B[i] = drand48();
      
    }
    
  }

  // col major form
  int col_major(int i, int j, int N){
    return j + i*N;
  }

  // Needed to rotate the filter for convolution
  void rotateMatrix(double *mat, double *new_mat, int N)
  {
    // iterate over number of rows
    for (int i = N-1; i >= 0; i--) {

      // iterate over the number of columns
      for (int j = N-1; j >= 0; j--) {

	// map the corresponding coordinates
	new_mat[col_major(N-1-i,N-1-j,N)] = mat[col_major(i,j,N)];
      }
    }
  }

  // Method to print matrix
  void print_matrix(double *mat, int M, int N){
    for (int i = 0; i < M; i++){
      for (int j = 0; j < N; j++){
	printf("(%d,%d): %f\n", i, j, mat[col_major(i, j, N)]);
      }
    }
  }

  // method to pad the original image
  void padding(double *input_img, double *out_img, int img_m, int img_n, int m_extra, int n_extra){

    
    // create variable to refer to dimensions of padded image
    int padded_m = img_m + m_extra;
    int padded_n = img_n + n_extra;
    
    // allocate memory based on size of new matrix
    // *out_img = (double *) calloc(padded_m * padded_n, sizeof(double));
    
    // update extra variables for purpose of looping indices 
    int m_extra_per_side = m_extra / 2;
    int n_extra_per_side = n_extra / 2;

    // loop over the number of rows in the new matrix wrt input_img coords
    for (int i = -m_extra_per_side; i < img_m + m_extra_per_side; i++){

      // loop over number of cols in the new matrix wrt input_img coords
      for (int j = -n_extra_per_side; j < img_n + n_extra_per_side; j++){

	// if indices are in input_img coords, update the value
	if (i >= 0 && i < img_m && j >= 0 && j < img_n){
	  
	  // update the value at the appropriate coordinates
	  out_img[col_major(i+m_extra_per_side,j+n_extra_per_side,padded_n)] = input_img[col_major(i,j,img_n)];
	}

	// If indices aren't valid, just add zeros
	else {

	  // insert zeros
	  out_img[col_major(i+m_extra_per_side, j+n_extra_per_side, padded_n)] = 0.0;
	}
      }
    }
  }

// Initialize the kernel values
  void initialize_window(double *window, double *I, int i, int j, int F_m, int F_n, int I_n){
    
    // iterate over number of rows for window
    for (int i_idx = i; i_idx < F_m + i; i_idx++){

      // iterate over number of columns for window
      for (int j_idx = j; j_idx < F_n + j; j_idx++){

	// extract the window of pixels in the original image's coordinates
	window[col_major(i_idx-i, j_idx-j, F_n)] =
	  I[col_major(i_idx, j_idx, I_n)];
      }
    }
  }

  // perform element wise multiplication
  void element_wise_mult(double *F, double *window, int i, int j,
			 int F_m, int F_n){
    
    // loop over rows
    for (int i = 0; i < F_m; i++){

      // loop over columns
      for (int j = 0; j < F_n; j++){

	// get corresponding index
	int idx = col_major(i,j,F_n);

	// perform element-wise multiplication and store results in window
	window[idx] *= F[idx];
      }
    }
  }

  // method that performs element-wise sum of two arrays
  // arrays have to be of the same length
  void element_wise_sum(double *output, double *input, int M, int N){

    // iterate over the number of columns
    for (int i = 0; i < M; i++){
      
      // iterate over number of rows
      for (int j = 0; j < N; j++){

	// perform element-wise summation
	output[col_major(i, j, N)] += input[col_major(i, j, N)];
      }
    }
  }
  
  // method to sum all elements of an array
  double sum_elements(double *arr, int F_n, int F_m){

    // accumulator
    double res = 0.0;

    // iterate over filter width
    for (int i = 0; i < F_m; i++){

      // iterate over filter height
      for (int j = 0; j < F_n; j++){

	// accumulate sum
	res += arr[col_major(i, j, F_n)];
      }
    }

    // return result
    return res;
  }
  
  // perform cross correlation
  void cross_correlation(double *output_img, double *I, double *F, int I_m, int I_n, int F_m, int F_n){

    // get dimensions for new image
    int m = I_m - F_m + 1;
    int n = I_n - F_n + 1;

    // initialize output image
    // double *output_img = (double *) calloc(m*n, sizeof(double));
    double *window = (double *) calloc(F_m * F_n, sizeof(double));

    // iterate over number of rows
    for (int i = 0; i < m; i++){

      // iterate over number of columns
      for (int j = 0; j < n; j++){

	// initialize window 
	initialize_window(window, I, i, j, F_m, F_n, I_n);

	// perform element-wise multiplication
	element_wise_mult(F, window, i, j, F_m, F_n);

	// sum up all the elements of the window
	output_img[col_major(i,j,n)] = sum_elements(window, F_m, F_n);
      }
    }    
  }


  void full_convolution(double *O, double *I, double *F, int I_m, int I_n, int F_m, int F_n){

    // get dimensions of the output image
    int n_extra = (F_m - 1) * 2;// I_n - F_n + 1;
    int m_extra = (F_n - 1) * 2;// I_m - F_m + 1;
    int padded_m = I_m + m_extra;
    int padded_n = I_n + n_extra;
    double *I_padded = (double *) calloc(padded_m * padded_n,
					 sizeof(double));
 
    // pad the matrix
    padding(I, I_padded, I_m, I_n, m_extra, n_extra);

    // initialize filter that is rotated 180 degrees
    double *F_rotated = (double *) calloc(F_m * F_n, sizeof(double));
    
    // rotate the kernel
    rotateMatrix(F, F_rotated, F_n);
    
    // perform cross correlation
    cross_correlation(O, I_padded, F_rotated, padded_m, padded_n, F_m, F_n);
    
  }

  // find kernel corresponding to starting and ending indices
  void find_local_arr(double *window, double *orig, int start_idx, int end_idx){

    // load the corresponding values into the local array
    for (int i = start_idx; i < end_idx; i++){
      window[i-start_idx] = orig[i]; 
    }
  }

  // read in data from one array to another
  void load_in(double *arr1, double *arr2, int start_idx, int end_idx){

    // same procedure as find_local_arr(), but not as obvious if you
    // look at the name, which is why this function is being made
    find_local_arr(arr1, arr2, start_idx, end_idx);
  }
  
  // implement the forward method
  // input is a matrix representing a single 2d image stored as a 1d array
  void forward(double *output, double *input){

    // initialize variables for indices used for creating a window
    int inp_start = -1;
    int inp_end = -1;
    int out_start = -1;
    int out_end = -1;
    int kernel_start = -1;
    int kernel_end = -1;
    int bias_start = -1;
    int bias_end = -1;

    // set up timer variables
    clock_t begin, end;
    double time_taken = 0.0;
    
    // create array that would represent a portion of the kernel
    double *local_kernel = (double *) calloc(kernel_size*kernel_size, sizeof(double));

    // create array that would represent a portion of the kernel
    double *local_bias = (double *) calloc(out_width*out_height, sizeof(double));

    // create array that would represent a portion of the kernel
    double *local_input = (double *) calloc(in_width*in_height, sizeof(double));
    
    // create array that would represent a portion of the output
    double *local_output = (double *) calloc(out_width*out_height, sizeof(double));

    // iterate over the depth of the input
    for (int i = 0; i < n_kernels; i++){

      // find starting and ending indices of output array
      out_start = col_major(i, 0, out_width*out_height);
      out_end = out_start + out_width*out_height;

      // find local region for output
      find_local_arr(local_output, output, out_start, out_end);
			       
      // iterate over the input depth
      for (int j = 0; j < in_channels; j++){

	// find starting and ending indices of bias
	bias_start = col_major(0, j, out_width*out_height);
	bias_end = bias_start + out_width*out_height;

	// find starting and ending indices of input
	inp_start = col_major(0, j, in_width*in_height);
	inp_end = inp_start + in_width*in_height;

	// get starting and ending indices of kernel
	kernel_start = col_major(i, j, kernel_size*kernel_size);
	kernel_end = kernel_start + kernel_size*kernel_size;
	
	// find local region for bias
	find_local_arr(local_bias, B, bias_start, bias_end);

	// find local region for input
	find_local_arr(local_input, input, inp_start, inp_end);
	
	// find local region for kernel
	find_local_arr(local_kernel, K, kernel_start, kernel_end);

	// begin timer
	begin = clock();
	
	// compute forward method
	cross_correlation(local_output, local_input, local_kernel, in_width, in_height, kernel_size, kernel_size);

	// end timer
	end = clock() - begin;

	// calcuate time taken
	time_taken += (double)(end)/CLOCKS_PER_SEC;

	// compute running sum
	element_wise_sum(local_output, local_output, out_width, out_height);	
      }

      // load local output data into corresponding portion of output matrix
      load_in(output, local_output, out_start, out_end);
    }

   // print results of timing
   printf("With the input image having dimensions (%d, %d, %d) and using %d kernels (no cuda), the total elapsed time is %f\n", INPUT_WIDTH, INPUT_HEIGHT, in_channels, n_kernels, time_taken);

  }
};
  
int main(int argc, char **argv){

  // set up parameters for ConvLayer
  int in_channels = atoi(argv[1]); //1
  int num_kernels = atoi(argv[2]); //5;
  int in_width = INPUT_WIDTH;
  int in_height = INPUT_HEIGHT;
  int kernel_size = KERNEL_SIZE;

  // test conv class
  ConvLayer conv = ConvLayer(num_kernels, in_width, in_height,
			     in_channels, kernel_size);
  
  // // number of rows and columns for the image, respectively
  // int M = 28;
  // int N = 28;
  // int f_m = 5;
  // int f_n = 5;

  // create output shape
  int out_width = in_width - kernel_size + 1;
  int out_height = in_height - kernel_size + 1;
  int out_channels = num_kernels;

  // instantiate input image
  double *I = (double *) calloc(in_channels*in_width*in_height,
			sizeof(double));

  // load in values for input image
  for(int c = 0; c < in_channels; c++){
    for (int i = 0; i < in_width; i++){
      for (int j = 0; j < in_height; j++){
	I[(c * in_height * in_width) + conv.col_major(i, j, in_height)] = conv.col_major(i, j, in_height);
      }
    }
  }


  // create new image
  double *O = (double *) calloc(out_channels*out_width*out_height,
				sizeof(double));

  // compute the forward method
  conv.forward(O, I);
}
