
#include <cuda.h>
#include <stdlib.h>
#include <stdio.h>

#define NX 32
#define NY 32
#define KERNEL_SIZE 16
#define INPUT_WIDTH 1024
#define INPUT_HEIGHT 768
#define OUTPUT_WIDTH INPUT_WIDTH-KERNEL_SIZE+1
#define OUTPUT_HEIGHT INPUT_HEIGHT-KERNEL_SIZE+1

// column major order
__device__ int row_major(int i, int j, int N){
  return j + i*N;
}

// function to print the matrix
__device__ void print_matrix(double *mat, int M, int N){

  for (int i = 0; i < M; i++){
    for (int j = 0; j < N; j++){
      printf("(%d,%d): %f\n", i, j, mat[row_major(i, j, N)]);
    }
  }  
}


// method to sum all elements of an array
__device__ double sum_elements(double arr[], int F_n, int F_m){

    // accumulator
    double res = 0.0;

    // iterate over filter width
    for (int i = 0; i < F_m; i++){

      // iterate over filter height
      for (int j = 0; j < F_n; j++){

	// accumulate sum
	res += arr[row_major(i, j, F_n)];
      }
    }

    // return result
    return res;
  }


// perform element wise multiplication
__device__ void element_wise_mult(double *F, double window[], int i,
				  int j, int F_m, int F_n){
    
    // loop over rows
    for (int i = 0; i < F_m; i++){

      // loop over columns
      for (int j = 0; j < F_n; j++){

	// get corresponding index
	int idx = row_major(i,j,F_n);

	// perform element-wise multiplication and store results in window
	window[idx] *= F[idx];
      }
    }
  }


// method to initialize the portion of the input data
// that will be used in the cross-correlation
__device__ void initialize_window(double window[], double *I, int i, int j, int F_m, int F_n, int I_n){

    // iterate over number of rows for window
    for (int i_idx = i; i_idx < F_m + i; i_idx++){

    // iterate over number of columns for window
      for (int j_idx = j; j_idx < F_n + j; j_idx++){

	// update window value
	window[row_major(i_idx-i, j_idx-j, F_n)] = I[row_major(i_idx, j_idx, I_n)];
      }
    }
}

// CUDA implementation of cross_correlation
__global__ void cross_correlate(double *I, double *F, double *O, int I_m, int I_n, int F_m, int F_n){

  // threadg information
  int tx = threadIdx.x;
  int ty = threadIdx.y;

  int dx = blockDim.x;
  int dy = blockDim.y;

  int bx = blockIdx.x;
  int by = blockIdx.y;

  int i = tx + bx*dx;
  int j = ty + by*dy;

  // get dimensions for new image
  int m = I_m - F_m + 1;
  int n = I_n - F_n + 1;
  
  // create window
  double window[KERNEL_SIZE*KERNEL_SIZE];
  
  // check if indices are valid
  if (i < m && j < n){
  
    // initialize the window
    initialize_window(window, I, i, j, F_m, F_n, I_n);

    // perform element-wise multiplication
    element_wise_mult(F, window, i, j, F_m, F_n);

    // sum up all the elements of the window
    O[row_major(i,j,OUTPUT_HEIGHT)] = sum_elements(window, F_m, F_n);

   }
   
}


int main(int argc, char **argv){

  // set device
  cudaSetDevice(2);

  // number of channels
  int In_channels = atoi(argv[1]);
  
  // number of kernels
  int N_kernels = atoi(argv[2]);
  
  // create dimensions for kernel
  int F_m = KERNEL_SIZE;
  int F_n = KERNEL_SIZE;
  
  // create dimensions for test image
  int I_m = INPUT_WIDTH;
  int I_n = INPUT_HEIGHT;

  // create dimensions for output width and height
  int O_m = OUTPUT_WIDTH;
  int O_n = OUTPUT_HEIGHT;
  
  // Initialize HOST arrays
  double *h_I  = (double*) calloc(In_channels*I_m*I_n, sizeof(double));
  double *h_F = (double*) calloc(F_m*F_n, sizeof(double));
  double *h_O = (double*) calloc(O_m*O_n, sizeof(double)); // sliding window
  
  // fill Image array
  // printf("Printing input array \n");
  for (int c = 0; c < In_channels; c++){
    for (int i = 0; i < I_m; i++){
      for (int j = 0; j < I_n; j++){
	h_I[(c * I_m * I_n) + (j + i*I_n)]= j + i*I_n;
	  // printf("(%d,%d): %d\n", i, j, c*I_m*I_n + j + i*I_n);
      	  }
      }
  }

  // fill Kernel
  // printf("Printing kernel\n");
  for (int i = 0; i < F_m; i++){
      for (int j = 0; j < F_n; j++){
      	  h_F[j + i*F_n]= j + i*F_n;
	  // printf("(%d,%d): %d\n", i, j, j + i*F_n);
	  }
      }

  // initialize DEVICE arrays
  double *c_I, *c_F, *c_O;
  cudaMalloc(&c_I, In_channels*I_m*I_n*sizeof(double));
  cudaMalloc(&c_F, F_m*F_n*sizeof(double));
  cudaMalloc(&c_O, O_m*O_n*sizeof(double));
  cudaMemcpy(c_I, h_I, I_m*I_n*sizeof(double), cudaMemcpyHostToDevice);
  cudaMemcpy(c_F, h_F, F_m*F_n*sizeof(double), cudaMemcpyHostToDevice);
  cudaMemcpy(c_O, h_O, O_m*O_n*sizeof(double), cudaMemcpyHostToDevice);
  
  // initialize chevrons
  dim3 B(NX,NY,1);
  dim3 G((NX+1)/NX, (NY+1)/NY, 1);

  // create timing info
  cudaEvent_t start, end;
  cudaEventCreate(&start);
  cudaEventCreate(&end);

  // track total amount of time (will be useful for finding average)
  double total_time = 0.0;

  // create event time array
  double *eventTimeArr = (double *) calloc(N_kernels * In_channels, sizeof(double));

  // test the cross_correlate function as if it were being used in forward()
  for (int i = 0; i < N_kernels * In_channels; i++){

    // Synchronize the device
    cudaDeviceSynchronize();

    // begin to record event
    cudaEventRecord(start);
    
    // call kernel
    cross_correlate <<< G,B >>> (c_I, c_F, c_O, I_m, I_n, F_m, F_n);

    // begin end record
    cudaEventRecord(end);

    // synchronize device
    cudaDeviceSynchronize();

    // get elapsed time
    float elapsedEvent;
    cudaEventElapsedTime(&elapsedEvent, start, end);
    elapsedEvent /= 1000;

    // add data to event time array
    eventTimeArr[i] = elapsedEvent;
    total_time += elapsedEvent;

    // retrieve cross-correlated data
    cudaMemcpy(h_O, c_O, O_m*O_n*sizeof(double), cudaMemcpyDeviceToHost);

  }

  // print output of forward matrix
  // printf("Printing cross-correlation output\n");
  // for (int i = 0; i < O_m; i++){
  //   for (int j = 0; j < O_n; j++){
  //     printf("(%d,%d): %f\n", i, j, h_O[j + i*O_n]);
  // 	}
  // }

  
  // print total elapsed time
  printf("With the input image having dimensions (%d, %d, %d) and using %d kernels (cuda), the total elapsed time is %f\n", INPUT_WIDTH, INPUT_HEIGHT, In_channels, N_kernels, total_time);  
  // exit
  return 0;
  }
